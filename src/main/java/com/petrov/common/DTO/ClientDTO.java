package com.petrov.common.DTO;

/**
 * ClientDTO class for WebService
 * Created by pavelpetrov on 15.11.16.
 */
public class ClientDTO {
    /**
     * client's first name
     */
    private String firstName;

    /**
     * client's second name
     */
    private String secondName;

    /**
     * client's address
     */
    private String clientAddress;

    /**
     * client's login
     */
    private String login;

    /**
     * client'd role
     */
    private String role;

    /**
     * role getter
     *
     * @return String role
     */
    public String getRole() {
        return role;
    }

    /**
     * role setter
     *
     * @param role String role
     */
    public void setRole(String role) {
        this.role = role;
    }

    /**
     * first name getter
     *
     * @return String first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * first name setter
     *
     * @param firstName String first name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * second name getter
     *
     * @return String second name
     */
    public String getSecondName() {
        return secondName;
    }

    /**
     * second name setter
     *
     * @param secondName String second name
     */
    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    /**
     * client's address getter
     *
     * @return String client's address
     */
    public String getClientAddress() {
        return clientAddress;
    }

    /**
     * client's address setter
     *
     * @param clientAddress String client's address
     */
    public void setClientAddress(String clientAddress) {
        this.clientAddress = clientAddress;
    }

    /**
     * client's login getter
     *
     * @return String client's login
     */
    public String getLogin() {
        return login;
    }

    /**
     * client's login setter
     *
     * @param login String client's login
     */
    public void setLogin(String login) {
        this.login = login;
    }


}
