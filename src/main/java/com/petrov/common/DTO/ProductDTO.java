package com.petrov.common.DTO;

/**
 * Product DTO class for WebService
 * Created by pavelpetrov on 17.11.16.
 */
public class ProductDTO {

    /**
     * product name
     */
    private String productName;

    /**
     * product price
     */
    private String productPrice;

    /**
     * product brand
     */
    private String productBrand;

    /**
     * prodcut name getter
     *
     * @return String  prodcut name
     */
    public String getProductName() {
        return productName;
    }

    /**
     * prodcut name setter
     *
     * @param productName String  prodcut name
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * product price getter
     *
     * @return String  product price
     */
    public String getProductPrice() {
        return productPrice;
    }

    /**
     * product price setter
     *
     * @param productPrice String  product price
     */
    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    /**
     * product Brand getter
     *
     * @return String product Brand
     */
    public String getProductBrand() {
        return productBrand;
    }

    /**
     * product Brand setter
     *
     * @param productBrand String product Brand
     */
    public void setProductBrand(String productBrand) {
        this.productBrand = productBrand;
    }
}
