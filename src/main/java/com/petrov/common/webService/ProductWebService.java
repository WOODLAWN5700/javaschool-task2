package com.petrov.common.webService;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.petrov.common.DTO.ProductDTO;
import org.apache.log4j.Logger;

import javax.ejb.Stateless;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Class contain Javax ws fro product's statistic
 * Created by pavelpetrov on 17.11.16.
 */
@Stateless
public class ProductWebService {

    /**
     * class logger
     */
    private static Logger logger = Logger.getLogger(ProductWebService.class);

    /**
     * get top product list from main App
     *
     * @return ProductDTO list
     */
    public List<ProductDTO> getTopProducts() {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080/service/getTopProducts");
        Response response = target
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        Gson gson = new Gson();
        String result = response.readEntity(String.class);
        Type listType = new TypeToken<ArrayList<ProductDTO>>() {
        }.getType();
        logger.info("created top ProductDAO list");
        return gson.fromJson(result, listType);
    }

    /**
     * get number of orders from main App
     *
     * @return Long list
     */
    public List<Long> getProductNumber() {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080/service/getNumberOfProduct");
        Response response = target
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        Gson gson = new Gson();
        String result = response.readEntity(String.class);
        Type listType = new TypeToken<ArrayList<Long>>() {
        }.getType();
        logger.info("crated list of Orders of product");
        return gson.fromJson(result, listType);
    }
}
