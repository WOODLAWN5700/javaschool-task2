package com.petrov.common.webService;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.petrov.common.DTO.ClientDTO;
import org.apache.log4j.Logger;

import javax.ejb.Stateless;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


/**
 * Class contain Javax ws fro client's statistic
 * Created by pavelpetrov on 15.11.16.
 */
@Stateless
public class ClientService {

    /**
     * class logger
     */
    private static Logger logger = Logger.getLogger(ClientService.class);


    /**
     * get top ClientsDTO list from URL
     * @return ClientDao LIst
     */
    public List<ClientDTO> getClients() {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080/service/getClients");

        Response response = target
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        Gson gson = new Gson();
        String result = response.readEntity(String.class);
        Type listType = new TypeToken<ArrayList<ClientDTO>>() {
        }.getType();
        logger.info("get ClientDTO List");
        return gson.fromJson(result, listType);
    }

    /**
     * get Long list of number of orders
     * @return
     */
    public List<Long> getClientsNumberOfOrder() {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080/service/nuberOfOrders");
        Response response = target
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        Gson gson = new Gson();
        String result = response.readEntity(String.class);
        Type listType = new TypeToken<ArrayList<Long>>() {
        }.getType();
        logger.info("get top list of orders of top clients");
        return gson.fromJson(result, listType);
    }




}
