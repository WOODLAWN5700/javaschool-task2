package com.petrov.common.webService;

import com.google.gson.Gson;

import javax.ejb.Stateless;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by pavelpetrov on 16.11.16.
 */
@Stateless
public class ProfitService {


    public Long getProfit(String firstDate, String secondDate) {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080/service/getProfit");
        Response response = target.queryParam("firstDate", firstDate).queryParam("secondDate", secondDate)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        Gson gson = new Gson();
        String result = response.readEntity(String.class);

        return gson.fromJson(result, Long.class);
    }
}
