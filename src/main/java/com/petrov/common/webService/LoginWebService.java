package com.petrov.common.webService;

import com.google.gson.Gson;
import com.petrov.common.DTO.ClientDTO;
import org.apache.log4j.Logger;

import javax.ejb.Stateless;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Login webService use Ajax ws
 * Created by pavelpetrov on 16.11.16.
 */
@Stateless
public class LoginWebService {

    /**
     * class logger
     */
    private static Logger logger = Logger.getLogger(LoginWebService.class);

    /**
     * login webService
     * @param login client's login
     * @param password client's password
     * @return Client DTO object, return null if client not in the DB
     */
    public ClientDTO getLoginClient(String login, String password){
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080/service/loginClient");

        Response response = target.queryParam("login", login).queryParam("password", password)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        Gson gson =  new Gson();
        String result = response.readEntity(String.class);
        return gson.fromJson(result, ClientDTO.class);
    }
}
