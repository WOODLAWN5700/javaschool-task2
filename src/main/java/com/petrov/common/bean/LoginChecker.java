package com.petrov.common.bean;

import com.petrov.common.DTO.ClientDTO;
import com.petrov.common.webService.LoginWebService;
import org.apache.log4j.Logger;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import java.io.IOException;

/**
 *Login bean for webServce
 * Created by pavelpetrov on 15.11.16.
 */
@ManagedBean
@SessionScoped
public class LoginChecker {

    /**
     * class logger
     */
    private static Logger logger = Logger.getLogger(LoginChecker.class);

    /**
     * Client's login
     */
    private String login;

    /**
     * Client's password
     */
    private String password;

    /**
     * inject LoginWebService bean
     */
    @EJB
    LoginWebService loginWebService;

    /**
     * get Client and chack what kind of role client has
     */
    public void checkClient(){
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext externalContext = context.getExternalContext();
        ClientDTO clientDTO = loginWebService.getLoginClient(login, password);
        try {
            if (clientDTO == null || !clientDTO.getRole().equals("ROLE_ADMIN")) {
                logger.info("Client is not admin");
                externalContext.redirect("error.xhtml");
            } else {
                logger.info("Client is Admin");
                externalContext.redirect("welcome.xhtml");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }

    /**
     * client's login getter
     * @return String client's login
     */
    public String getLogin() {
        return login;
    }

    /**
     * client's login setter
     * @param login String client's login
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * client's password getter
     * @return client's password
     */
    public String getPassword() {
        return password;
    }

    /**
     * client's password setter
     * @param password client's password
     */
    public void setPassword(String password) {
        this.password = password;
    }

}
