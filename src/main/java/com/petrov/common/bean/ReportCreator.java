package com.petrov.common.bean;

import com.petrov.common.webService.ProfitService;
import org.apache.log4j.Logger;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

/**
 * praraer infromation for PDF
 * Created by pavelpetrov on 16.11.16.
 */

@ManagedBean
@ViewScoped
public class ReportCreator {

    /**
     * class logger
     */
    private static Logger logger = Logger.getLogger(ReportCreator.class);

    /**
     * first date
     */
    private String firstDate;

    /**
     * second date
     */
    private String secondDate;

    /**
     * profit
     */
    private Long profit;

    /**
     * inject PDFCreator bean
     */
    @EJB
    private PDFCreator pdfCreator;

    /**
     * inject ProfitService bean
     */
    @EJB
    ProfitService profitService;

    /**
     * prfit getter from bean
     */
    public void getProfitFromApp() {
        Long prifitList = profitService.getProfit(firstDate, secondDate);
        setProfit(prifitList);
    }

    /**
     * create statistic for admin
     */
    public void getStatistic() {
        getProfitFromApp();
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
        logger.info("prepared information for PDFcreator");
        pdfCreator.createPDF(response, firstDate, secondDate, profit);
        context.responseComplete();
    }

    /**
     * first date getter
     *
     * @return first date
     */
    public String getFirstDate() {
        return firstDate;
    }

    /**
     * first date setter
     *
     * @param firstDate first date
     */
    public void setFirstDate(String firstDate) {
        this.firstDate = firstDate;
    }

    /**
     * secont date getter
     *
     * @return second date
     */
    public String getSecondDate() {
        return secondDate;
    }

    /**
     * secont date setter
     *
     * @param secondDate second date
     */
    public void setSecondDate(String secondDate) {
        this.secondDate = secondDate;
    }

    /**
     * profit getter
     *
     * @return Long profit
     */
    public Long getProfit() {
        return profit;
    }

    /**
     * profit setter
     *
     * @param profit Long profit
     */
    public void setProfit(Long profit) {
        this.profit = profit;
    }
}
