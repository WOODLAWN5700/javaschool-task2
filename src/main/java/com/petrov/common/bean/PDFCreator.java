package com.petrov.common.bean;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.petrov.common.DTO.ClientDTO;
import com.petrov.common.DTO.ProductDTO;
import com.petrov.common.webService.ClientService;
import com.petrov.common.webService.ProductWebService;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.bean.ManagedBean;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/**
 * PDF creator bean
 * Created by pavelpetrov on 16.11.16.
 */
@ManagedBean
@Stateless
public class PDFCreator {

    /**
     * inject ClientService bean
     */
    @EJB
    ClientService clientService;

    /**
     * inject ProductWebService bean
     */
    @EJB
    ProductWebService productWebService;

    /**
     * Contains all the specifications of a font: fontfamily, size, style and color
     */
    private Font font;

    /**
     * PDFCreator constructor
     * @throws DocumentException
     * @throws IOException
     */
    public PDFCreator() throws DocumentException, IOException {
        setFont();
    }

    /**
     * create PDF file for webService
     * @param response HTTPServletResponce
     * @param firstDate first date
     * @param secndDate second Date
     * @param profit statistic profit
     */
    public void createPDF(HttpServletResponse response,String firstDate, String secndDate, Long profit) {
        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition","attachment; filename=\"statistic.pdf\"");

        List<ClientDTO> clientDTOs = clientService.getClients();
        List<Long> numberOfOrders = clientService.getClientsNumberOfOrder();
        List<ProductDTO> productDTOs = productWebService.getTopProducts();
        List<Long> numberOfProducts = productWebService.getProductNumber();

        Document document = new Document(PageSize.A4.rotate(), 50, 50, 50, 50);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        OutputStream os = null;

        try {
            PdfWriter.getInstance(document, baos);

            document.open();

            Image img = Image.getInstance("logo.png");
            img.setAbsolutePosition(380,0);

            document.add(img);

            Paragraph paragraph1 = new Paragraph("Profit from " + firstDate + " to " + secndDate  + " is " + profit,
                    FontFactory.getFont(FontFactory.HELVETICA, 16, Font.BOLD,
                            new CMYKColor(0, 255, 0, 0)));
            paragraph1.setAlignment(Element.ALIGN_CENTER);
            document.add(paragraph1);


            com.itextpdf.text.List list = new com.itextpdf.text.List(true, false, 10);
            list.add("Top clinets of E-SHOP");
            document.add(list);

            PdfPTable table = new PdfPTable(5);
            table.setWidthPercentage(100);
            table.setSpacingBefore(20);
            table.setSpacingAfter(20);

            PdfPCell cell1 = new PdfPCell(new Phrase("First Name", font));
            table.addCell(cell1);

            PdfPCell cell2 = new PdfPCell(new Phrase("Last Name", font));
            table.addCell(cell2);

            PdfPCell cell3 = new PdfPCell(new Phrase("Client's login", font));
            table.addCell(cell3);

            PdfPCell cell4 = new PdfPCell(new Phrase("Clieent's Address", font));
            table.addCell(cell4);

            PdfPCell cell5 = new PdfPCell(new Phrase("Number Of orders", font));
            table.addCell(cell5);


            int a = 0;
            for (ClientDTO  client : clientDTOs){
                addCell(table, client.getFirstName());
                addCell(table, client.getSecondName());
                addCell(table, client.getLogin());
                addCell(table, client.getClientAddress());
                addCell(table, numberOfOrders.get(a).toString());
                a++;
            }

            document.add(table);

            com.itextpdf.text.List list2 = new com.itextpdf.text.List(true, false, 10);
            list2.add("Top products of E-SHOP");
            document.add(list2);
            PdfPTable table2 = new PdfPTable(4);
            table2.setWidthPercentage(100);
            table2.setSpacingBefore(20);
            table2.setSpacingAfter(20);

            PdfPCell param1 = new PdfPCell(new Phrase("Product Name", font));
            table2.addCell(param1);

            PdfPCell param2 = new PdfPCell(new Phrase("Prodcut Price", font));
            table2.addCell(param2);

            PdfPCell param3 = new PdfPCell(new Phrase("Product Brand", font));
            table2.addCell(param3);

            PdfPCell param4 = new PdfPCell(new Phrase("Number Of Orders", font));
            table2.addCell(param4);

            int proctNumberId = 0;
            for (ProductDTO  productDTO : productDTOs){
                addCell(table2, productDTO.getProductName());
                addCell(table2, productDTO.getProductPrice());
                addCell(table2, productDTO.getProductBrand());
                addCell(table2, numberOfProducts.get(proctNumberId).toString());
                proctNumberId++;
            }

            document.add(table2);


            document.close();

            response.setHeader("Expires", "0");
            response.setHeader("Cache-Control","must-revalidate, post-check=0, pre-check=0");
            response.setHeader("Pragma", "public");
            response.setContentLength(baos.size());

            os = response.getOutputStream();
            baos.writeTo(os);
            os.flush();

        } catch (DocumentException | IOException e) {
            e.printStackTrace();
        } finally {
            if (os != null){
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     *  A cell in a PdfPTable.
     * @param table table in pdf file
     * @param value value in rable
     * @return cell
     */
    private PdfPCell addCell(PdfPTable table, String value) {
        PdfPCell cell = new PdfPCell(new Paragraph(value, font));
        table.addCell(cell);
        return cell;
    }

    /**
     * set from style
     * @throws DocumentException
     * @throws IOException
     */
    private void setFont() throws DocumentException, IOException {
        BaseFont baseFont = BaseFont.createFont("ArialRegular.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
        font = new Font(baseFont);
    }

}
